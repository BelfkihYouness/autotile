﻿using System;
using System.Drawing;
using System.IO;
using System.Linq;

namespace autotile
{
    static class Program
    {

        private static Bitmap source;
        private static int unit;
        private static Graphics g;

        private static int[,] tiles = new int[,]
        {

            { 4, 4, 4, 4 },
            { 3, 5, 6, 8 },
            { 1, 2, 7, 8 },
            { 9, 5, 7, 8 },
            { 4, 5, 7, 8 },
            { 0, 1, 6, 7 },
            { 3, 10, 6, 7 },
            { 3, 4, 6, 7 },


            { 1, 1, 7, 7 },
            { 9, 10, 7, 7 },
            { 4, 10, 7, 7 },
            { 9, 4, 7, 7 },
            { 4, 4, 7, 7 },
            { 0, 2, 3, 5 },
            { 3, 5, 3, 5 },
            { 1, 2, 12, 5 },


            { 9, 5, 12, 5 },
            { 4, 5, 12, 5 },
            { 0, 1, 3, 13 },
            { 3, 10, 3, 13 },
            { 3, 4, 3, 13 },
            { 1, 1, 12, 13 },
            { 9, 10, 12, 13 },
            { 4, 10, 12, 13 },

            { 9, 4, 12, 13 },
            { 4, 4, 12, 13 },
            { 1, 2, 4, 5 },
            { 9, 5, 4, 5 },
            { 4, 5, 4, 5 },
            { 1, 1, 4, 13 },
            { 9, 10, 4, 13 },
            { 4, 10, 4, 13 },


            { 9, 4, 4, 13 },
            { 4, 4, 4, 13 },
            { 0, 1, 3, 4 },
            { 3, 10, 3, 4 },
            { 3, 4, 3, 4 },
            { 1, 1, 12, 4 },
            { 9, 10, 12, 4 },
            { 4, 10, 12, 4 },

            { 9, 4, 12, 4 },
            { 4, 4, 12, 4 },
            { 1, 1, 4, 4 },
            { 9, 10, 4, 4 },
            { 4, 10, 4, 4 },
            { 9, 4, 4, 4 },
            { 4, 4, 4, 4 },
            { 0, 2, 6, 8 },

        };

        static void Main(string[] args)
        {
            if (args.Length != 2)
            {
                PrintUsage();
                Console.ResetColor();
                return;
            }

            var sourcename = args[0];

            var sourceext = Path.GetExtension(sourcename).ToLower();

            if (sourceext != ".png")
            {
                PrintUsage();
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($" - Source file extension {sourceext} is not supported.\n");
                Console.ResetColor();
                return;
            }

            if (!File.Exists(sourcename))
            {
                PrintUsage();
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($" - source file {sourcename} does not exist.\n");
                Console.ResetColor();
                return;
            }

            var targetname = args[1];

            if (File.Exists(targetname))
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine($"\n ! File {targetname} already exists, overriding.\n");
                Console.ResetColor();
            }

            source = new Bitmap(sourcename);

            unit = source.Width / 3;

            var result = new Bitmap(unit * 16, unit * 12);
            g = Graphics.FromImage(result);

            for (int ti = 0; ti < tiles.GetLength(0); ti++)
            {

                var tileX = (ti % 8) * 2;
                var tileY = (ti / 8) * 2;

                for (int si = 0; si < 4; si++) {

                    var destX = tileX + si % 2;
                    var destY = tileY + si / 2;

                    var source = tiles[ti, si];

                    var sourceX = source % 3;
                    var sourceY = source / 3;

                    DrawSprite(destX, destY, sourceX, sourceY);

                }

            }

            result.Save(targetname);

        }

        private static void PrintUsage()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"\n Usage: autotile [input] [output]\n");
        }

        static void DrawSprite(int tx, int ty, int sx, int sy)
        {
            g.DrawImage(
                source,
                new Rectangle(tx * unit, ty * unit, unit, unit),
                new Rectangle(sx * unit, sy * unit, unit, unit),
                GraphicsUnit.Pixel
            );
        }

    }
}
